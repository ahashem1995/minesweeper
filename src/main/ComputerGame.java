package main;

import models.Cell;
import models.Computer;
import models.Player;
import settings.Action;

import java.util.ArrayList;
import java.util.List;

public class ComputerGame extends Game {
    @Override
    protected void initPlayers() {
        this.players.add(new Player("Your"));
        this.players.add(new Computer(this.grid));
        this.currentPlayer = this.players.getFirst();
        this.stage.setTitle(this.currentPlayer.getName() + "'s (" + this.currentPlayer.getScore().getValue()
                + ") Turn");
    }

    @Override
    public void onCellOpened(Cell cell) {
        this.updatePlayerScore(cell.getValue(), Action.OPEN);
        super.changeTurn();
        super.checkGameOver(cell.getX(), cell.getY());
        if (!this.over && this.currentPlayer.getName().equals("Computer")) {
            this.currentPlayer.makeMove(this.shuffledCoordinates);
        }
    }

    @Override
    public void onCellFlag(Cell cell) {
        super.onCellFlag(cell);
        if (!this.over && this.currentPlayer.getName().equals("Computer")) {
            this.currentPlayer.makeMove(this.shuffledCoordinates);
        }
    }

    @Override
    protected String getWinnersTitle() {
        StringBuilder winnersTitle = new StringBuilder();
        List<Player> winners = new ArrayList<>();
        int maxScore = Integer.MIN_VALUE;
        for (Player player : this.players) {
            if (player.getScore().getValue() >= maxScore) {
                winners.add(player);
                maxScore = player.getScore().getValue();
            }
        }
        if (winners.size() == 0) {
            winnersTitle.append("No player");
        } else {
            for (Player player : winners) {
                winnersTitle.append(player.getName()).append(" (").append(player.getScore().getValue()).append(") ");
            }
        }
        winnersTitle.append("won");
        return winnersTitle.toString();
    }
}
