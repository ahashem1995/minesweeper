package main;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Pair;
import models.Cell;
import models.Player;
import settings.Action;
import settings.Settings;
import settings.Status;

import java.util.*;
import java.util.List;

public abstract class Game extends Application {
    /**
     * Game over or not
     */
    public boolean over = false;
    /**
     * Root container pane
     */
    private Pane root;
    /**
     * Represents window that contains the pane
     */
    Stage stage;
    /**
     * Number of bombs specified by user
     */
    private int bombsCount;
    /**
     * Top-left three numbers of remaining bombs
     * Rectangle for each image (ones, tens, hundreds)
     */
    private Rectangle[] remainingBombs = new Rectangle[3];
    /**
     * Current player (turn)
     */
    Player currentPlayer;
    /**
     * Game players represented by a linked list
     */
    LinkedList<Player> players = new LinkedList<>();
    /**
     * Grid cells (tiles)
     */
    Cell[][] grid = new Cell[Settings.Y_CELLS][Settings.X_CELLS];
    /**
     * List of grid random coordinates
     * [(3, 1), (1, 2), (5, 7), (4, 6) ..]
     */
    List<Pair<Integer, Integer>> shuffledCoordinates = new ArrayList<>();

    /**
     * Initialize the game and show the game window
     * @param parent pane root to add children to it
     */
    public void start(Parent parent) {
        this.stage = new Stage();
        this.initGame((Pane) parent);
        this.stage.setScene(new Scene(parent));
        this.stage.setResizable(false);
        this.stage.sizeToScene();
        this.stage.show();
    }

    /**
     * Initialize Grid, Players, Remaining Bombs, Faces
     * @param root pane root to add children to it
     */
    private void initGame(Pane root) {
        this.root = root;
        this.initGrid(root);
        this.initPlayers();
        this.initRemainingBombs();
        this.initFace();
    }

    /**
     * 1. Initializes game grid with new cells and add them to root pane
     *    all cells doesn't have bombs by default
     * 2. Randomly shuffle the coordinates (x, y) in list of pairs
     *    get first Settings.BOMBS_COUNT random coordinates and set hasBomb to true
     * 3. Initialize each cell to set it's value (number)
     * @param root pane root to add children to it
     */
    private void initGrid(Pane root) {
        Cell cell;
        for (int y = 0; y < Settings.Y_CELLS; y++) {
            for (int x = 0; x < Settings.X_CELLS; x++) {
                this.shuffledCoordinates.add(new Pair<>(y, x));
                cell = new Cell(x, y, false, this);
                this.grid[y][x] = cell;
                root.getChildren().add(cell);
            }
        }
        // Random
        Collections.shuffle(this.shuffledCoordinates);
        Pair<Integer, Integer> coordinate;
        for (int i = 0; i < Settings.BOMBS_COUNT; i++) {
            coordinate = this.shuffledCoordinates.get(i);
            this.grid[coordinate.getKey()][coordinate.getValue()].hasBomb = true;
            this.grid[coordinate.getKey()][coordinate.getValue()].setValue(Status.GRAY_BOMBED);
        }
        for (int y = 0; y < Settings.Y_CELLS; y++) {
            for (int x = 0; x < Settings.X_CELLS; x++) {
                cell = this.grid[y][x];
                // Calculate number
                cell.init(this.grid);
            }
        }
    }

    /**
     * Initializes tob centered faces as stack panes
     * On face click -> reset grid to a new game
     */
    private void initFace() {
        StackPane stackPane = new StackPane();
        Rectangle rectangle = new Rectangle(1.5 * Settings.CELL_SIZE, 1.5 * Settings.CELL_SIZE);
        rectangle.setFill(Settings.ImageService.getImage(Status.SMILEY));
        stackPane.getChildren().add(rectangle);
        stackPane.setTranslateX(Settings.WIDTH / 2.0);
        stackPane.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                this.reset();
            }
        });
        this.root.getChildren().add(stackPane);
    }

    /**
     * Reset all game parameters (Start a new one)
     */
    private void reset() {
        this.root.getChildren().clear();
        this.players.clear();
        this.currentPlayer = null;
        this.over = false;
        this.stage.setTitle(Settings.WINDOW_TITLE);
        this.initGame(this.root);
    }

    /**
     * Initializes top left remaining bombs
     */
    private void initRemainingBombs() {
        this.bombsCount = Settings.BOMBS_COUNT;
        double width = 0.75, height = 1.5;

        StackPane onesDigit = new StackPane();
        StackPane tensDigit = new StackPane();
        StackPane hundredsDigit = new StackPane();

        Rectangle onesRectangle = new Rectangle(width * Settings.CELL_SIZE, height * Settings.CELL_SIZE);
        Rectangle tensRectangle = new Rectangle(width * Settings.CELL_SIZE, height * Settings.CELL_SIZE);
        Rectangle hundredsRectangle = new Rectangle(width * Settings.CELL_SIZE, height * Settings.CELL_SIZE);

        this.remainingBombs[0] = onesRectangle;
        this.remainingBombs[1] = tensRectangle;
        this.remainingBombs[2] = hundredsRectangle;

        hundredsDigit.setTranslateX(width * Settings.CELL_SIZE);
        tensDigit.setTranslateX(2 * width * Settings.CELL_SIZE);
        onesDigit.setTranslateX(3 * width * Settings.CELL_SIZE);

        onesRectangle.setFill(Settings.ImageService.getImage(
                Status.getDisplayNumber(this.bombsCount % 10)));
        tensRectangle.setFill(Settings.ImageService.getImage(
                Status.getDisplayNumber((this.bombsCount / 10) % 10)));
        hundredsRectangle.setFill(Settings.ImageService.getImage(
                Status.getDisplayNumber(this.bombsCount / 100)));

        onesDigit.getChildren().add(onesRectangle);
        tensDigit.getChildren().add(tensRectangle);
        hundredsDigit.getChildren().add(hundredsRectangle);

        this.root.getChildren().add(hundredsDigit);
        this.root.getChildren().add(tensDigit);
        this.root.getChildren().add(onesDigit);
    }

    /**
     * Update player score by action taken on a specified cell
     * Score is updated due to action taken and cell real value (not displayed one)
     * @param value of cell clicked
     * @param action taken on cell
     */
    void updatePlayerScore(Status value, Action action) {
        try {
            this.currentPlayer.getScore().update(value, action);
            System.out.println(this.currentPlayer.getName() + "'s score = " + this.currentPlayer.getScore());
        } catch (NullPointerException e) {
            // Player not found
            System.err.println(e.getMessage());
        }
    }

    /**
     * Update remaining bombs count due to flag action or open
     * @param isFlagged action true or false
     */
    private void updateRemainingBombs(boolean isFlagged) {
        this.bombsCount = isFlagged ? (this.bombsCount - 1) : (this.bombsCount + 1);
        int onesDigit = this.bombsCount % 10;
        int tensDigit = ((this.bombsCount / 10) % 10);
        int hundredsDigit = this.bombsCount / 100;

        this.remainingBombs[0].setFill(Settings.ImageService.getImage(Status.getDisplayNumber(onesDigit)));
        this.remainingBombs[1].setFill(Settings.ImageService.getImage(Status.getDisplayNumber(tensDigit)));
        this.remainingBombs[2].setFill(Settings.ImageService.getImage(Status.getDisplayNumber(hundredsDigit)));
    }

    /**
     * Remove negative score players
     * if there is no players then game will end
     * @param cellX on which clicked
     * @param cellY on which clicked
     */
    void checkGameOver(int cellX, int cellY) {
        Player player;
        Iterator<Player> iterator = this.players.iterator();
        while (iterator.hasNext()) {
            player = iterator.next();
            if (!player.getScore().isPositive()) {
                iterator.remove();
            }
        }
        if (this.players.size() == 0) {
            this.endGame(cellX, cellY);
            return;
        }
        Cell cell;
        for (int y = 0; y < Settings.Y_CELLS; y++) {
            for (int x = 0; x < Settings.X_CELLS; x++) {
                cell = this.grid[y][x];
                if (cell.hasBomb) {
                    if (!cell.getIsFlagged())
                        return;
                } else {
                    if (!cell.getIsOpened())
                        return;
                }
            }
        }
        this.over = true;
        this.stage.setTitle(this.currentPlayer.getName() + " won!");
    }

    /**
     * Open all remaining cells
     * @param cellX on which clicked
     * @param cellY on which clicked
     */
    private void endGame(int cellX, int cellY) {
        for (int y = 0; y < Settings.Y_CELLS; y++) {
            for (int x = 0; x < Settings.X_CELLS; x++) {
                this.grid[y][x].setOnMouseClicked(null);
                if ((x != cellX && y != cellY) && this.grid[y][x].hasBomb) {
                    this.grid[y][x].open();
                }
            }
        }
        this.over = true;
        this.stage.setTitle(this.getWinnersTitle());
    }

    /**
     * Change turn to next player in linked list
     */
    void changeTurn() {
        Iterator<Player> iterator = this.players.iterator();
        while (iterator.hasNext()) {
            if (this.currentPlayer == iterator.next()) {
                this.currentPlayer = (iterator.hasNext()) ? iterator.next() : this.players.getFirst();
                break;
            }
        }

        String title = this.currentPlayer.getName();
        if (this.currentPlayer.getScore().getValue() < Settings.DISPLAY_SCORE_THRESHOLD)
            title += " (" + this.currentPlayer.getScore().getValue() + ")";
        title += " Turn";
        this.stage.setTitle(title);
    }

    /**
     * Event that is fired whenever a cell is opened
     * 1. Current player score is updated
     * 2. Turn change to next player
     * 3. Check if game is over
     * @param cell which is clicked
     */
    public void onCellOpened(Cell cell) {
//        this.currentPlayer.getScore().update(cell.getValue(), Action.OPEN);
        this.updatePlayerScore(cell.getValue(), Action.OPEN);
        this.changeTurn();
        this.checkGameOver(cell.getX(), cell.getY());
    }

    /**
     * Event that is fired whenever a cell is flagged
     * 1. Current player score is updated
     * 2. Turn change to next player
     * 3. Check if game is over
     * 4. Update remaining bombs number
     * @param cell which is clicked
     */
    public void onCellFlag(Cell cell) {
        this.updatePlayerScore(cell.getValue(), cell.getIsFlagged() ? Action.FLAG : Action.UN_FLAG);
        this.changeTurn();
        this.checkGameOver(cell.getX(), cell.getY());
        this.updateRemainingBombs(cell.getIsFlagged());
    }

    @Override
    public void start(Stage primaryStage) {}

    protected abstract void initPlayers();

    protected abstract String getWinnersTitle();
}
