package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import main.ComputerGame;
import main.Game;
import main.PlayerGame;
import settings.Settings;

import java.net.URL;
import java.util.ResourceBundle;

public class MainMenuController implements Initializable {

    @FXML
    CheckBox custom;

    @FXML
    TextField width;
    @FXML
    TextField height;
    @FXML
    TextField bombs;
    @FXML
    TextField playersCount;

    @FXML
    RadioButton singleMode;
    @FXML
    RadioButton multiPlayerMode;
    @FXML
    RadioButton vsComputerMode;

    private ToggleGroup toggleGroup = new ToggleGroup();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.initRadioButtons();
        this.initSettingsValues();
    }

    public void onPlayButtonClicked() {
        Settings.update(Integer.parseInt(this.width.getText()), Integer.parseInt(this.height.getText()),
                Integer.parseInt(this.playersCount.getText()), Integer.parseInt(this.bombs.getText()));

        Game game = (this.toggleGroup.getSelectedToggle().getUserData().equals("vsComputerMode"))
                ? new ComputerGame()
                : new PlayerGame();
        game.start(this.createMainContent());
    }

    /**
     * Main options pane
     * @return Pane
     */
    private Pane createMainContent() {
        Pane root = new Pane();
        root.setStyle("-fx-background-color: #7f7f7f;");
        root.setPrefSize(Settings.WIDTH, Settings.HEIGHT + (1.5 * Settings.CELL_SIZE));
        return root;
    }

    private void initRadioButtons() {
        this.singleMode.setUserData("singleMode");
        this.singleMode.setToggleGroup(this.toggleGroup);
        this.multiPlayerMode.setUserData("multiPlayerMode");
        this.multiPlayerMode.setToggleGroup(this.toggleGroup);
        this.vsComputerMode.setUserData("vsComputerMode");
        this.vsComputerMode.setToggleGroup(this.toggleGroup);

        // Enable/Disable player count field
        this.toggleGroup.selectedToggleProperty().addListener(((observable, oldValue, newValue) -> {
            boolean isMultiPlayer = newValue.getUserData().equals("multiPlayerMode");
            this.playersCount.setDisable(!isMultiPlayer);
        }));
    }

    /**
     * Toggle (enable/disable) fields on custom checkbox clicked
     */
    public void customOnAction() {
        this.toggleFields(!this.custom.isSelected());
    }

    private void toggleFields(boolean selected) {
        this.initSettingsValues();
        this.width.setDisable(selected);
        this.height.setDisable(selected);
        this.bombs.setDisable(selected);
    }

    /**
     * Set width & height with settings values
     */
    private void initSettingsValues() {
        this.width.setText(String.valueOf(Settings.X_CELLS));
        this.height.setText(String.valueOf(Settings.Y_CELLS));
        this.bombs.setText(String.valueOf(Settings.BOMBS_COUNT));
        this.playersCount.setText(String.valueOf(Settings.PLAYERS_COUNT));
    }
}
