package models;

import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import main.Game;
import settings.Settings;
import settings.Status;

import java.util.ArrayList;
import java.util.List;

public class Cell extends StackPane {
    /**
     * x coordinate at pane
     */
    private int x;
    /**
     * y coordinate at pane
     */
    private int y;
    /**
     * Default value for each cell
     */
    private Status value = Status.COVERED;
    /**
     * Indicator if cell has a bomb or not
     */
    public boolean hasBomb;
    /**
     * Game that has this cell
     */
    private Game game;
    /**
     * Indicator if cell is opened
     */
    private boolean isOpened = false;
    /**
     * Indicator if cell is flagged
     */
    private boolean isFlagged = false;
    /**
     * List of Cells around this cell
     */
    private List<Cell> neighbors;
    /**
     * Rectangle that displays some image
     */
    private Rectangle rectangle = new Rectangle(Settings.CELL_SIZE, Settings.CELL_SIZE);

    public Cell(int x, int y, boolean hasBomb, Game game) {
        this.x = x;
        this.y = y;
        this.hasBomb = hasBomb;
        if (this.hasBomb) this.value = Status.GRAY_BOMBED;
        this.game = game;
        this.init();
    }

    /**
     * Sets neighbors of this cell
     * Count it's value
     * @param grid array of cells
     */
    public void init(Cell[][] grid) {
        this.neighbors = new ArrayList<>();
        // Neighbors dx & dy points
        int[] points = new int[] {
                1, 1,
                1, 0,
                1, -1,
                0, -1,
                -1, -1,
                -1, 0,
                -1, 1,
                0, 1,
        };

        int dx, dy, neighborX, neighborY;
        for (int i = 0; i < points.length; i++) {
            dx = points[i];
            dy = points[++i];
            neighborX = this.x + dx;
            neighborY = this.y + dy;
            if (Settings.isValidCoordinates(neighborX, neighborY)) {
                this.neighbors.add(grid[neighborY][neighborX]);
            }
        }
        if (!this.hasBomb) {
            this.setValue(this.neighbors.stream().filter(c -> c.hasBomb).count());
        }
    }

    /**
     * Initializes a new cell
     */
    private void init() {
        this.initRectangle();
        this.initPosition();
        this.listenOnMouseEvents();
    }

    /**
     * Initializes rectangle that displays image for a cell
     * MUST be added to children of current cell
     */
    private void initRectangle() {
        // Displayed image
        this.rectangle.setFill(Settings.ImageService.getImage(Status.COVERED));
        this.getChildren().add(this.rectangle);
    }

    /**
     * Translate cell by it's coordinates
     */
    private void initPosition() {
        this.setTranslateX(this.x * Settings.CELL_SIZE);
        this.setTranslateY((this.y + 1.5) * Settings.CELL_SIZE);
    }

    /**
     * Listen on mouse pressed, released and clicked
     */
    private void listenOnMouseEvents() {
        // On mouse middle button pressed but not released
        this.setOnMousePressed(event -> {
            if (event.getButton() == MouseButton.MIDDLE) {
                if (!this.isOpened && !this.isFlagged) this.rectangle.setFill(Settings.ImageService.getImage(Status.BLANK));
                this.neighbors.stream().filter(n -> !n.isOpened && !n.isFlagged)
                        .forEach(n -> n.rectangle.setFill(Settings.ImageService.getImage(Status.BLANK)));
            }
        });
        // On mouse middle button released
        this.setOnMouseReleased(event -> {
            if (event.getButton() == MouseButton.MIDDLE) {
                if (!this.isOpened && !this.isFlagged) this.rectangle.setFill(Settings.ImageService.getImage(Status.COVERED));
                this.neighbors.stream().filter(n -> !n.isOpened && !n.isFlagged)
                        .forEach(n -> n.rectangle.setFill(Settings.ImageService.getImage(Status.COVERED)));
            }
        });
        // On left or right mouse clicked
        this.setOnMouseClicked(event -> {
            if (this.game.over) return;
            if (event.getButton() == MouseButton.PRIMARY) {
                this.onLeftClick();
            } else if (event.getButton() == MouseButton.SECONDARY) {
                this.onRightClick();
            }
        });
    }

    /**
     * Event fired when mouse left button is clicked
     */
    void onLeftClick() {
        if (this.hasBomb) this.value = Status.BOMBED;
        if (this.open()) this.game.onCellOpened(this);
    }

    /**
     * Open cell if it's not flagged
     * @return
     */
    public boolean open() {
        if (this.isFlagged) return false;
        this.isOpened = true;
        if (this.value != Status.BLANK) {
            this.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.MIDDLE) {
                    // Get flagged neighbors count
                    if (this.value.getValue() == this.neighbors.stream().filter(neighbor -> neighbor.isFlagged).count()) {
                        this.neighbors.stream().filter(neighbor -> !neighbor.isOpened).forEach(Cell::onLeftClick);
                    }
                }
            });
        }

        this.rectangle.setFill(Settings.ImageService.getImage(this.value));
        // Flood fill
        if (this.value == Status.BLANK) {
            this.neighbors.stream().filter(neighbor -> !neighbor.isOpened).forEach(Cell::open);
        }
        return true;
    }

    /**
     * Event fired on right clicked
     */
    void onRightClick() {
        this.rectangle.setFill(Settings.ImageService.getImage(this.isFlagged ?
                                                                Status.COVERED : Status.FLAGGED));
        this.isFlagged = !this.isFlagged;
        this.game.onCellFlag(this);
    }

    public boolean getIsFlagged() {
        return this.isFlagged;
    }

    public boolean getIsOpened() { return this.isOpened; }

    private void setValue(long value) {
        this.value = Status.getValue((int) value);
    }

    public void setValue(Status status) { this.value = status; }

    public Status getValue() {
        return this.value;
    }

    public int getY() {
        return this.y;
    }

    public int getX() {
        return this.x;
    }
}