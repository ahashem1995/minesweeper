package models;

import javafx.util.Pair;

import java.util.List;

public class Player {
    private String name;
    private Score score;

    public Player(String name) {
        this(name, new Score());
    }

    public Player(String name, Score score) {
        this.name = name;
        this.score = score;
    }

    public void makeMove(List<Pair<Integer, Integer>> shuffledCoordinates) {}

    public String getName() {
        return name;
    }

    public Score getScore() {
        return this.score;
    }
}
