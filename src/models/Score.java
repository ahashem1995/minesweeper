package models;

import settings.Action;
import settings.Status;

public class Score {
    private int value;

    Score() {
        this(0);
    }

    private Score(int value) {
        this.value = value;
    }

    public void update(Status status, Action action) {
        // Game rules
        if (action == Action.OPEN) {
            if (Status.isNumber(status)) {
                this.value += status.getValue();
            }
            if (status == Status.BLANK) {
                this.value += 10;
            }
            if (status == Status.BOMBED) {
                this.value += -250;
            }
        }
        if (action == Action.FLOOD) {
            this.value += 1;
        }
        if (action == Action.FLAG) {
            if (status == Status.GRAY_BOMBED) {
                this.value += 5;
            } else {
                this.value += -1;
            }
        }
        if (action == Action.UN_FLAG) {
            if (status == Status.GRAY_BOMBED) {
                this.value -= 5;
            }
        }
    }

    public int getValue() {
        return value;
    }

    public boolean isPositive() {
        return this.value >= 0;
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }
}
